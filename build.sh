#!/bin/bash -ex

# CONFIG
prefix="Skype"
suffix=""
munki_package_name="Skype"
icon_name=""
category="Social Networking"
description="Voice-over-internet phone and chat software."
display_name="Skype"
#url=`./finder.sh`
url="https://go.skype.com/skype.download"


# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

#mount downloaded DMG
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
# /usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R $app_in_dmg build-root/Applications

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" $app_in_dmg/Contents/Info.plist`
#identifier=`/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" $app_in_dmg/Contents/Info.plist`
# (cd build-root; pax -rz -f ../pkg/*/Payload)
hdiutil detach "${mountpoint}"

## Create pkg's
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.Skype --install-location / --version $version app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
# Build pkginfo
#/usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg > app.plist

plist=`pwd`/app.plist

# Obtain version info
# Mount disk image on temp space
# version=`defaults read "${plist}" version`

#mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`
#echo Mounted on $mountpoint
#cp "$mountpoint/Skype.app/Contents/Info.plist" skype.plist
#pwd=`pwd`
#version=`defaults read "$pwd/skype.plist" CFBundleVersion`
#minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`
#hdiutil detach "$mountpoint"
#rm -rf skype.plist

# Set version comparison key
/usr/libexec/PlistBuddy -c "Set :installs:0:version_comparison_key CFBundleVersion" app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" blocking_applications -array "Skype.app"
defaults write "${plist}" unattended_install -bool YES
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" unattended_install -bool TRUE

# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist

