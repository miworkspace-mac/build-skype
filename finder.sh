#!/bin/bash

#download dmg
curl -L -o Skype.dmg -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36" "https://go.skype.com/mac.download"

#mount downloaded DMG
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse Skype.dmg | awk '/private\/tmp/ { print $3 } '`

#locate app within downloaded DMG
app_in_dmg=$(ls -d $mountpoint/*.app)

# Obtain version info from APP
VERSION=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" $app_in_dmg/Contents/Info.plist`

if [ "x${VERSION}" != "x" ]; then
	echo "${VERSION}"
fi

hdiutil detach -quiet "${mountpoint}"

rm -rf Skype.dmg

